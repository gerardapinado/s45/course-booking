let courses = [
    {
        id: "wd001",
        name: "PHP-Laravel",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra sit amet aliquam id diam maecenas ultricies mi. Senectus et netus et malesuada fames.",
        price: 40000,
        onOffer: true
    },
    {
        id: "wd002",
        name: "Python-Django",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra sit amet aliquam id diam maecenas ultricies mi. Senectus et netus et malesuada fames.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wd003",
        name: "Java-Springboot",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pharetra sit amet aliquam id diam maecenas ultricies mi. Senectus et netus et malesuada fames.",
        price: 50000,
        onOffer: false
    },
    {
		id: "wdc004",
		name: "NodeJS-ExpressJS",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea tenetur illo, delectus doloribus consequuntur facere exercitationem laborum blanditiis magnam sequi iste",
		price: 55000,
		onOffer: false
	}
]

export default courses