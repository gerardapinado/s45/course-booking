import { Fragment, useState } from "react"


export default function CourseCard({courseProp}){

    let [count, setCount] = useState(0)
    let [seats, setSeat] = useState(30)
    const {name, description, price} = courseProp

    const handleClick = () => {
        if(count<30){
            setCount(++count)
            setSeat(--seats)
            console.log(`Count: ${count}`)
            console.log(`Seats: ${seats}`)
        }
        else{
            alert(`No more seats.`)
        }
        
    }

    return(
        <Fragment>
        <div className="card" style={{width: '18rem'}}>
        <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h6 className="card-subtitle mb-2 text-muted">Description</h6>
            <p className="card-text">{description}</p>
            <p className="card-text m-0">Price:</p>
            <p className="card-text">PHP: {price}</p>
            <p className="card-text">Count: {count}</p>
            <button className="btn btn-info" onClick={handleClick}>Enroll</button>
            {/* <a href="#" className="btn btn-primary card-link">Enroll</a> */}
        </div>
        </div>
        </Fragment>
    )
}