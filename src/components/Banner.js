import { Fragment } from "react"
import { useNavigate } from "react-router-dom"

export default function Banner({pageProp}){
    let {isFound} = pageProp
    let navigate = useNavigate()
    return(
      <Fragment>
        {
        pageProp.status==false
        ?
          <div className="jumbotron">
            <h1 className="display-4">Page Not Found!</h1>
            <p className="lead">Go back to the <a href='/'>homepage</a>.</p>
          </div>
        :
          <div className="jumbotron">
            <h1 className="display-4">Welcome to Course Booking App!</h1>
            <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <a className="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </div>
        }
      </Fragment>
    
    )
}

// export default function BannerProps({bannerProp}){
//   const {title,desc,destination,btnDesc} = bannerProp

//   return(
//     <Fragment>
//       <div className="jumbotron">
//         <h1 className="display-4">{title}</h1>
//         <p className="lead">{desc}</p>
//         <a className="btn btn-primary btn-lg" href={destination} role="button">{btnDesc}</a>
//       </div>
//     </Fragment>
    
//   )
// }