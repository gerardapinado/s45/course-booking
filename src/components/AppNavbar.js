import { Fragment, useContext } from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap'
import UserContext from '../userContext'

export default function AppNavBar(){
  // const {user} = useContext(UserContext)
  let email = localStorage.getItem('email')

    return(
    <Nav activeKey="/home">
      <Nav.Item>
        <Nav.Link href="/">Home</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/courses">Courses</Nav.Link>
      </Nav.Item>
      {
        // user.email !== null
        email !== null 
        ?
        <Nav.Item>
        <Nav.Link href="/logout">Logout</Nav.Link>
        </Nav.Item>
        :
        <Fragment>
          <Nav.Item>
          <Nav.Link href="/login">Login</Nav.Link>
          </Nav.Item>
          <Nav.Item>
          <Nav.Link href="/register">Register</Nav.Link>
          </Nav.Item>
        </Fragment>
      }
    </Nav>
    )
}