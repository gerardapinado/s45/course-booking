import { Fragment, useContext } from "react"
import CourseCard from "../components/CourseCard"
import courses from "../mockDB/coursesDB"
import UserContext from "../userContext"


export default function Courses(){
    const {user, setUser} = useContext(UserContext)
    console.log(user)

    const coursesData = courses.map(data => {
        return <CourseCard key={courses.id} courseProp={data}/>
    })

    return(
        <Fragment>
            {coursesData}
        </Fragment>
    )
}