import { Fragment, useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import UserContext from "../userContext"


export default function Register(){
    const [fName, setFName] = useState("")
    const [lName, setLName] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [cpw, setCpw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()

    useEffect(() => {
        // console.log(`Render`)
        if(fName!=="" && lName!=="" && email!=="" && pw!=="" && cpw!==""){
            if(pw==cpw){
                setIsDisabled(false)
            }
        }
        else {
            setIsDisabled(true)
        }
    }, [fName, lName, email, pw, cpw])

    const registerUser = e => {
        e.preventDefault()
        
        fetch('http:/localhost:3007/api/users/email-exist', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(res => {
            if(!res){
                fetch('http:/localhost:3007/api/users/register', {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: email,
                        password: pw
                    })
                })
                .then(res => res.json())
                .then(res => {
                    if(res){
                        alert(`Registration Successful.`)
                        // Redirect User
                        navigate('/login')
                    }
                    else{
                        alert(`Login Failed. Please try again`)
                    }
                })
            }
        })
    }

    const registerUserNoRoute = e => {
        navigate('/login')
    }

    const {user,setUser} = useContext(UserContext)
    useEffect(() => {
        if(user.isAdmin!=null){
            navigate('/courses')
        }
    },[])


    return(
        <Fragment>
            
            <div class="col-12 col-md-6">
							<form onSubmit={(e) => registerUserNoRoute(e)} id="register">
								<div class="form-group">
									<label for="firstname">First name:</label>
									<input value={fName} onChange={(e) => setFName(e.target.value)} type="text" id="firstname" required class="form-control"/>
								</div>

								<div class="form-group">
									<label for="lastname">Last name:</label>
									<input value={lName} onChange={(e) => setLName(e.target.value)} type="text" id="lastname" required class="form-control"/>
								</div>

								<div class="form-group">
									<label for="email">Email:</label>
									<input value={email} onChange={(e) => setEmail(e.target.value)} type="email" id="email" required class="form-control"/>
								</div>

								<div class="form-group">
									<label for="password">Password:</label>
									<input value={pw} onChange={(e) => setPw(e.target.value)} type="password" id="password" required class="form-control"/>
								</div>
								<div class="form-group">
									<label for="cpw">Confirm Password:</label>
									<input value={cpw} onChange={(e) => setCpw(e.target.value)} type="password" id="cpw" required class="form-control"/>
								</div>
								<button type="submit" class="btn btn-info" disabled={isDisabled}>Submit</button>
							</form>

			</div>
            
        </Fragment>
    )
}