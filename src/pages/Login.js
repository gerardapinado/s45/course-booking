import { Fragment, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'


export default function Login(){
    const [email, setEmail] = useState("")
    const [pass, setPass] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)
    let mockData = {
        email: "admin@mail.com",
        pass: "admin"
    }
    const navigate = useNavigate()

    useEffect(() => {
        if(email!=="" && pass!=""){
            setIsDisabled(false)
        }
        else{
            setIsDisabled(true)
        }
    },[email, pass])

    const loginUserNoRoute = e => {
        e.preventDefault()
        if(email==mockData.email && pass==mockData.pass){
            alert(`You are now logged in`)
            localStorage.setItem('email', email)
            navigate('/courses')
        }
        else{
            alert(`Login Failed.`)
        }
    }

    const loginUser = e => {
        e.preventDefault()

        fetch('http://localhost:3007/api/users/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: pass
            })
        })
        .then(res => res.json())
        .then(res => {
            console.log(res)

            if(res){
                localStorage.setItem('token', res.token)
                localStorage.setItem('email', email)

                alert(`Login Successfully`)
                setEmail("")
                setPass("")
                navigate('/courses')
            }
            else{
                alert(`Incorrect Credentials!`)
            }
            
        })
    }

    return(
        <div className="col-12 col-md-6">
            <form onSubmit={e => loginUserNoRoute(e)} id="login">
                <div className="form-group">
                    <label for="email">Email:</label>
                    <input value={email} onChange={e => setEmail(e.target.value)} type="email" id="email" placeholder="admin@mail.com" required className="form-control"/>
                </div>

                <div className="form-group">
                    <label for="password">Password:</label>
                    <input value={pass} onChange={e => setPass(e.target.value)} type="password" id="password" placeholder='admin' required className="form-control"/>
                </div>
                <button type="submit" className="btn btn-info" disabled={isDisabled}>Submit</button>
            </form>

        </div>
    )
}