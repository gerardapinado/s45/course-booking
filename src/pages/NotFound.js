import { Fragment } from "react"
import Banner from "../components/Banner"
// import BannerProps from "../components/Banner"


export default function NotFound(){
    let isFound = {
        status: false
    }
    return(
        <Fragment>
            <Banner pageProp={isFound}/>
        </Fragment>
    )
}

// export default function ErrorPage(){
//     const data = {
//         title: "Error 404",
//         description: "Page not found",
//         destination: "/",
//         btnDesc: "Go back home"
//     }

//     return <BannerProps bannerProp={data}/>
// }