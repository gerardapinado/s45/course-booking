import Banner from './../components/Banner'
// import Footer from '../components/Footer'
// import AppNavBar from '../components/AppNavbar'
// import CourseCard from '../components/CourseCard'
import { Fragment } from 'react'

// import BannerProps from '../components/Banner'

export default function Home(){
    let isFound = {
        status: true
    }
    return(
        <Fragment>
            {/* Navbar Component */}
            {/* <AppNavBar/> */}
            {/* Banner Component */}
            <Banner pageProp={isFound}/>
            {/* <CourseCard/> */}
            {/* Footer Component */}
            {/* <Footer/> */}
        </Fragment>
    )
}

// export default function HomeProp(){
//     const data = {
//         title: "Welcome to Course Booking",
//         description: "Opportunities for everyone, everywhere",
//         destination: "/courses",
//         btnDesc: "Check Courses"
//     }

//     return <BannerProps bannerProp={data}/>
// }