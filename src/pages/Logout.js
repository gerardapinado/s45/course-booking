import { useContext, useEffect } from "react"
import UserContext from "../userContext"
import { Navigate, useNavigate } from "react-router-dom"

export default function Logout() {
    const navigate = useNavigate()
    console.log(localStorage)

    const {user,setUser} = useContext(UserContext)

    useEffect(() => {
        setUser({
            id:null,
            isAdmin:null
        })
        localStorage.clear()
        // navigate('/login')
    }, [])

    return( <Navigate to="/login" />)
}