import { Fragment, useEffect, useState } from "react";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import NotFound from "./pages/NotFound";

import { UserProvider } from "./userContext";

import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import AppNavBar from "./components/AppNavbar";
import Footer from "./components/Footer";


function App() {
const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const token = localStorage.getItem('token')
useEffect( () => {
  fetch('http://localhost:3007/api/users/', {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${token}`
    }
  })
  .then(res => res.json())
  .then(res => {
    setUser({
      id: res._id,
      isAdmin: res.isAdmin,
      email: res.email
    })
  }) 
}, [])

  return(
    <UserProvider value={{user, setUser}} >
      <BrowserRouter>
        <AppNavBar/>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            {/* {
            (user.isAdmin!=null)
            ?
            <Route path="/register" element={<Courses/>}/> */}
            {/* : */}
            <Route path="/register" element={<Register/>}/>  
            {/* } */}
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path='*' element={<NotFound/>}/>
          </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>
  )
}

export default App;
